package org.raptors;

public class ThreadTest2 {

	static int i = 0;

	public static void main(String[] args) {

		final Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("主线程thread开始执行");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
				System.out.println("主线程thread执行完毕");
			}
		});
		Thread thread_1 = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("thread_1开始执行");
				try {
					Thread.sleep(1000);
					System.out.println("thread_1执行完毕");
				} catch (InterruptedException e) {
				}
				if (setValue() == 3) {
					thread.start();
				}
			}
		});
		Thread thread_2 = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("thread_2开始执行");
				try {
					Thread.sleep(1000);
					System.out.println("thread_2执行完毕");
				} catch (InterruptedException e) {
				}
				if (setValue() == 3) {
					thread.start();
				}
			}
		});
		Thread thread_3 = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("thread_3开始执行");
				try {
					Thread.sleep(1000);
					System.out.println("thread_3执行完毕");
				} catch (InterruptedException e) {
				}
				if (setValue() == 3) {
					thread.start();
				}
			}
		});
		thread_1.start();
		thread_2.start();
		thread_3.start();
	}

	private static synchronized int setValue() {
		return ++i;
	}
}
