package org.raptors;

import java.util.ArrayList;
import java.util.List;

public class ThreadTest1 {

	static int i = 0;
	static List<Integer> totalList = new ArrayList<Integer>();

	public static void main(String[] args) throws InterruptedException {

		final ThreadTest1 t = new ThreadTest1();//对象锁
		final Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				synchronized (t) {
					try {
						t.wait();
						System.out.println("主线程thread开始执行");
						Thread.sleep(500);
						for (Integer i : totalList) {
							System.err.print(i+".");
						}
						System.out.println("主线程thread执行完毕");
					} catch (InterruptedException e) {
					}
				}
			}
		});
		Thread thread_1 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("thread_1开始执行");
					Thread.sleep(500);
					List<Integer> list = new ArrayList<Integer>();
					list.add(1);
					addList(list);
					System.out.println("thread_1执行完毕");
					synchronized (t) {
						if (setValue() == 3)
							t.notify();
					}
				} catch (Exception e) {
				}
			}
		});
		Thread thread_2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("thread_2开始执行");
					Thread.sleep(500);
					List<Integer> list = new ArrayList<Integer>();
					list.add(2);
					addList(list);
					System.out.println("thread_2执行完毕");
					synchronized (t) {
						if (setValue() == 3)
							t.notify();
					}
				} catch (Exception e) {
				}
			}
		});
		Thread thread_3 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("thread_3开始执行");
					Thread.sleep(500);
					List<Integer> list = new ArrayList<Integer>();
					list.add(3);
					addList(list);
					System.out.println("thread_3执行完毕");
					synchronized (t) {
						if (setValue() == 3)
							t.notify();
					}
				} catch (Exception e) {
				}
			}
		});
		thread.start();
		thread_1.start();
		thread_2.start();
		thread_3.start();
	}

	private static synchronized int setValue() {
		return ++i;
	}

	private static void addList(List<Integer> list) {
		totalList.addAll(list);
	}
}
